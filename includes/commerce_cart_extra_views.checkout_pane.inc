<?php

/**
 * Settings form to choose views displays.
 * @param
 *   $checkout_pane
 * @return 
 *   Form Array
 */
function commerce_cart_extra_views_settings_form($checkout_pane) {
  $form = array();
  $arg = arg();
  $view_id = isset($arg[6]) ? check_plain($arg[6]) : NULL;

  //Check empty for if $view_id not available.
  if (!$view_id)
    return $form;

  $defined_view = views_get_view($view_id);

  // Build an options array of Views available for the cart contents pane.
  $options = array();

  $default = variable_get($defined_view->name, 'default');

  if ($defined_view) {
    $view_id = $defined_view->name;
    foreach ($defined_view->display as $display_id => $display_value) {
      $key = $view_id . '|' . $display_id;
      $options[$view_id][$view_id . '|' . $display_id] = $display_value->display_title;
    }
  }

  $form[$defined_view->name] = array(
    '#type' => 'select',
    '#title' => t($defined_view->human_name),
    '#description' => t('Select specific display of %view view.', array('%view' => $defined_view->human_name)),
    '#options' => $options,
    '#default_value' => $default,
  );

  return $form;
}

/**
 * Commerce Cart Extra Views Pane
 * checkout form.
 */
function commerce_cart_extra_views_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $pane_form = array();
  if ($checkout_pane['pane_id']) {
    // Extract the View and display keys from the cart contents pane setting.
    list($view_id, $display_id) = explode('|', variable_get($checkout_pane['pane_id'], 'default'));
    $pane_form[$checkout_pane['pane_id']] = array(
      '#markup' => commerce_cart_extra_views_embed_view($view_id, $display_id),
    );
  }
  return $pane_form;
}

/**
 * Renders a View for display in some other element.
 *
 * @param $view_key
 *   The ID of the View to embed.
 * @param $display_id
 *   The ID of the display of the View that will actually be rendered.
 * @param $arguments
 *   An array of arguments to pass to the View.
 * @param $override_url
 *   A url that overrides the url of the current view.
 *
 * @return
 *   The rendered output of the chosen View display.
 */
function commerce_cart_extra_views_embed_view($view_id, $display_id, $arguments = NULL, $override_url = NULL) {
  // Load the specified View.
  $view = views_get_view($view_id);
  $view->set_display($display_id);

  // Set the specific arguments passed in.
  if ($arguments) {
    $view->set_arguments($arguments);
  }
  // Override the view url, if an override was provided.
  if ($override_url) {
    $view->override_url = $override_url;
  }

  // Prepare and execute the View query.
  $view->pre_execute();
  $view->execute();

  // Return the rendered View.
  return $view->render();
}
