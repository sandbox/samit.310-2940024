<?php

/**
 * @file
 * Admin pages for Commerce Cart Extra Views.
 */

/**
 * Form of settings for the extra views.
 */
function commerce_cart_extra_views_settings_form($form, &$form_state) {
  $form = array();

  $form['commerce_cart_extra_views'] = array(
    '#type' => 'textarea',
    '#title' => t('Commerce Cart Extra Views'),
    '#default_value' => variable_get('commerce_cart_extra_views', NULL),
    '#description' => t("A list of views machine name, one on each line."),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
