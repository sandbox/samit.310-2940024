CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Commerce Cart Extra Views module allows you to place one or more views as checkout panes for
Drupal Commerce.
Commerce Cart Extra Views provide checkout panes for views selected with different display id's.

FEATURES
--------
* Option to enter views machine name.
* Option to select entered views displays in checkout pane configuration setting page. 

REQUIREMENTS
------------
 * Views (http://drupal.org/project/views)
 * commerce (http://drupal.org/project/commerce)
 * commerce_checkout (http://drupal.org/project/commerce)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 Go to Administration » Store » Configuration » Checkout settings:
   URL: admin/commerce/config/checkout/views-extra

   * Add views machine name(A list of views machine name, one on each line.).
   * Go to Checkout settings (Administration » Store » Configuration)
     URL: admin/commerce/config/checkout
   * Configure the views pane setting in Disabled region.
   * Select specific display of entered view in CHECKOUT PANE CONFIGURATION block and Save configuration.
   * Move these PANE any specific region.
